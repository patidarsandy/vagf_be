package com.vegf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.vegf.module.files.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class VegFApplication {

	public static void main(String[] args) {
		SpringApplication.run(VegFApplication.class, args);
	}

}

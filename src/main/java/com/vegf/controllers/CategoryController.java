package com.vegf.controllers;

import java.util.List;
import java.util.logging.Level;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vegf.entities.Category;
import com.vegf.services.CategoryService;
import com.vegf.vm.MessageVM;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	CategoryService categoryService;
	
//	@RequestMapping(value = "", method = RequestMethod.POST)
//	@ResponseBody
//	public MessageVM createCategory(@RequestBody Category category){
//		try {
//			return categoryService.createCategory(category);
//		} catch (Exception e) {
//			MessageVM messageVM = new MessageVM();
//			messageVM.setCode("500");
//			messageVM.setMessage(e.getMessage());
//			return messageVM;
//		}
//	}

	@RequestMapping(value = "/api/save", method = RequestMethod.POST)
	@ResponseBody
	public MessageVM createCategory(@RequestParam("model") String model,  @RequestParam(value = "file", required = false) MultipartFile file, @RequestParam(value = "files", required = false) List<MultipartFile> files){
		try {
			return categoryService.createCategory(model, file, files);
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}

	
	@RequestMapping(value = "/api/all", method = RequestMethod.GET)
	@ResponseBody
	public MessageVM getAllCategory() {
		try {
			return categoryService.getAllCategory();
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@ResponseBody
	public MessageVM saveCategory(@RequestBody Category category){
		try {
			return categoryService.saveCategory(category);
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}
	
	@RequestMapping(value = "/{catId}/{active}", method = RequestMethod.PATCH)
	@ResponseBody
	public MessageVM updateCategoryStatus(@PathParam("catId")Long catId,@PathParam("active")Boolean active){
		try {
			return categoryService.updateCategoryStatus(catId,active);
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}
}

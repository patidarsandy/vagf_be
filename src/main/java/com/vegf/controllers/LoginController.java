package com.vegf.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vegf.services.LoginService;
import com.vegf.vm.MessageVM;
import com.vegf.vm.UserProfileVM;

@RestController
@RequestMapping("/user")
public class LoginController {
	private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

	@Autowired
	LoginService loginService;
	

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	@ResponseBody
	public MessageVM createUser(@RequestBody UserProfileVM userProfileVM){
        
		return loginService.createUser(userProfileVM);
	}

	@RequestMapping(value = "/start", method = RequestMethod.GET)
	@ResponseBody
	public String started(){
        
		return "Started";
	}
	
}

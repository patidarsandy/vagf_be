package com.vegf.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vegf.services.ProductService;
import com.vegf.vm.MessageVM;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	ProductService productService;

	@RequestMapping(value = "/api/save", method = RequestMethod.POST)
	@ResponseBody
	public MessageVM addProduct(@RequestParam("model") String model,  @RequestParam(value = "file", required = false) MultipartFile file, @RequestParam(value = "files", required = false) List<MultipartFile> files){
		try {
			return productService.addProduct(model, file, files);
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}

	
	@RequestMapping(value = "/api/all", method = RequestMethod.GET)
	@ResponseBody
	public MessageVM getAllProduct() {
		try {
			return productService.getAllProduct();
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}
}

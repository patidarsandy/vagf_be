package com.vegf.controllers;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vegf.entities.Category;
import com.vegf.entities.SubCategory;
import com.vegf.services.CategoryService;
import com.vegf.services.SubCategoryService;
import com.vegf.vm.MessageVM;

@RestController
@RequestMapping("/subcategory")
public class SubCategoryController {

	@Autowired
	SubCategoryService subCategoryService;
	
//	@RequestMapping(value = "", method = RequestMethod.POST)
//	@ResponseBody
//	public MessageVM createSubCategory(@RequestBody SubCategory subCategory){
//		try {
//			return subCategoryService.createSubCategory(subCategory);
//		} catch (Exception e) {
//			MessageVM messageVM = new MessageVM();
//			messageVM.setCode("500");
//			messageVM.setMessage(e.getMessage());
//			return messageVM;
//		}
//	}
	
	@RequestMapping(value = "/api/save", method = RequestMethod.POST)
	@ResponseBody
	public MessageVM createSubCategory(@RequestParam("model") String model,  @RequestParam(value = "file", required = false) MultipartFile file, @RequestParam(value = "files", required = false) List<MultipartFile> files){
		try {
			return subCategoryService.createSubCategory(model, file, files);
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}

	@RequestMapping(value = "/api/all", method = RequestMethod.GET)
	@ResponseBody
	public MessageVM getAllSubCategory() {
		try {
			return subCategoryService.getAllSubCategory();
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	@ResponseBody
	public MessageVM saveSubCategory(@RequestBody SubCategory subCategory){
		try {
			return subCategoryService.saveSubCategory(subCategory);
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}
	
	@RequestMapping(value = "/{catId}/{active}", method = RequestMethod.PATCH)
	@ResponseBody
	public MessageVM updateSubCategoryStatus(@PathParam("catId")Long catId,@PathParam("active")Boolean active){
		try {
			return subCategoryService.updateSubCategoryStatus(catId,active);
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}
}

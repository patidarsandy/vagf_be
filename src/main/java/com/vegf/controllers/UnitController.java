package com.vegf.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vegf.services.UnitService;
import com.vegf.vm.MessageVM;

@RestController
@RequestMapping("/unit")
public class UnitController {
	
	@Autowired
	UnitService unitService;
	
	@RequestMapping(value = "/api/all", method = RequestMethod.GET)
	@ResponseBody
	public MessageVM getAllProduct() {
		try {
			return unitService.getAllUnit();
		} catch (Exception e) {
			MessageVM messageVM = new MessageVM();
			messageVM.setCode("500");
			messageVM.setMessage(e.getMessage());
			return messageVM;
		}
	}
}

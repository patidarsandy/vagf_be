package com.vegf.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vegf.entities.auth.AuthUser;

@Repository
@Transactional
@Component
public class LoginDao {
	@Autowired
	SessionFactory sessionFactory;
	
	public void createUser(AuthUser authUser)
	{
		Session session = sessionFactory.getCurrentSession(); 
	    session.save(authUser);
	}
	
	public AuthUser getUser(String emailId)
	{
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria =session.createCriteria(AuthUser.class);
		criteria.add(Restrictions.eq("email", emailId));
		return (AuthUser)criteria.uniqueResult();
		
	}
	
	public AuthUser getUserById(Long id)
	{
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria =session.createCriteria(AuthUser.class);
		criteria.add(Restrictions.eq("id", id));
		return (AuthUser)criteria.uniqueResult();
		
	}
	public AuthUser getAuthUserByEmailId(String email)
	{
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(AuthUser.class);
		criteria.add(Restrictions.eq("username", email));
		return (AuthUser)criteria.uniqueResult();
	}
	

	
}

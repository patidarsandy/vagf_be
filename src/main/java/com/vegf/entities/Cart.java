package com.vegf.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="cart")
@JsonIgnoreProperties(ignoreUnknown = true,value = {"hibernateLazyInitializer", "handler" })
public class Cart {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id" )
	private Long id;

	@OneToOne(fetch=FetchType.LAZY,orphanRemoval=false)
	@JoinColumn(name="fk_product_id")
	private Product product;
	
	@OneToOne(fetch=FetchType.LAZY,orphanRemoval=false)
	@JoinColumn(name="fk_user_id")
	private User user;

	@Column(name="quantity", columnDefinition = "Decimal(18,4) default 0")
	private Double quantity;

	@Column(name="price", columnDefinition = "Decimal(18,4) default 0")
	private Double price;
	
	@Column(name="discount_in_per", columnDefinition = "Decimal(18,4) default 0")
	private Double discountInPer;

	@Column(name="discount_amt", columnDefinition = "Decimal(18,4) default 0")
	private Double discountAmount;

	@Column(name="sale_price")
	private Double salePrice = 0d;
	
	@Column(name="isPlaced", columnDefinition = "tinyint(1) default 0")
	private Boolean isPlaced = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getDiscountInPer() {
		return discountInPer;
	}

	public void setDiscountInPer(Double discountInPer) {
		this.discountInPer = discountInPer;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	public Boolean getIsPlaced() {
		return isPlaced;
	}

	public void setIsPlaced(Boolean isPlaced) {
		this.isPlaced = isPlaced;
	}


}

package com.vegf.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="category" )
@JsonIgnoreProperties(ignoreUnknown = true,value = {"hibernateLazyInitializer", "handler" })
public class Category {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id" )
	private Long id;
	
	@Column(name="category_name", length=255)
	private String categoryName;
	
	@Column(name="category_description", length=255)
	private String categoryDescription;
	
	@Column(name="icon_path", length=255)
	private String iconPath;
	
	@Column(name="active", columnDefinition = "tinyint(1) default 0")
	private Boolean active = false;

	@Column(name="discount", columnDefinition = "Decimal(18,4) default 0")
	private Double discount;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "fk_category_id", referencedColumnName = "id")
    private List<CategoryImages> categoryImages = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	
	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<CategoryImages> getCategoryImages() {
		return categoryImages;
	}

	public void setCategoryImages(List<CategoryImages> categoryImages) {
		this.categoryImages = categoryImages;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
	
}

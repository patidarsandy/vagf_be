package com.vegf.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="ordered_product" )
@JsonIgnoreProperties(ignoreUnknown = true,value = {"hibernateLazyInitializer", "handler" })
public class OrderedProduct {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id" )
	private Long id;

	@Column(name="product_name", length=255)
	private String productName;

	@Column(name="quantity", columnDefinition = "Decimal(18,4) default 0")
	private Double quantity;
	
	@Column(name="unit_value", columnDefinition = "Decimal(18,4) default 0")
	private Double unitValue;
	
	@OneToOne(fetch=FetchType.LAZY,orphanRemoval=false)
	@JoinColumn(name="fk_unit_id")
	private Unit unit;

	@Column(name="price", columnDefinition = "Decimal(18,4) default 0")
	private Double price;
	
	@Column(name="discount", columnDefinition = "Decimal(18,4) default 0")
	private Double discount;
	
	@Column(name="total_amount", columnDefinition = "Decimal(18,4) default 0")
	private Double totalAmount;
	
	@OneToOne(fetch=FetchType.LAZY,orphanRemoval=false)
	@JoinColumn(name="fk_product_id")
	private Product product;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getUnitValue() {
		return unitValue;
	}

	public void setUnitValue(Double unitValue) {
		this.unitValue = unitValue;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	
	
}

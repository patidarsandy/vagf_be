package com.vegf.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="orders" )
@JsonIgnoreProperties(ignoreUnknown = true,value = {"hibernateLazyInitializer", "handler" })
public class Orders {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id" )
	private Long id;
	
	@Column(name="order_id", length=1000)
	private String orderId;

	@Column(name="no_of_items", columnDefinition = "Decimal(18,4) default 0")
	private Double noOfItems;

	@Column(name="product_names", length=1000)
	private String productNames;

	@Column(name="total_amount", columnDefinition = "Decimal(18,4) default 0")
	private Double totalAmount;

	@Column(name = "order_date", nullable = false, columnDefinition = "DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderDate;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "fk_orders_id", referencedColumnName = "id")
    private List<OrderedProduct> orderedProduct = new ArrayList<>();

	@OneToOne(fetch=FetchType.LAZY,orphanRemoval=false)
	@JoinColumn(name="fk_payment_summary_id")
	private PaymentSummary paymentSummary;

	@OneToOne(fetch=FetchType.LAZY,orphanRemoval=false)
	@JoinColumn(name="fk_refund_amount_details_id")
	private RefundAmountDetails refundAmountDetails;

	@OneToOne(fetch=FetchType.LAZY,orphanRemoval=false)
	@JoinColumn(name="fk_delivery_address_id")
	private DeliveryAddress deliveryAddress;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Double getNoOfItems() {
		return noOfItems;
	}

	public void setNoOfItems(Double noOfItems) {
		this.noOfItems = noOfItems;
	}

	public String getProductNames() {
		return productNames;
	}

	public void setProductNames(String productNames) {
		this.productNames = productNames;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public List<OrderedProduct> getOrderedProduct() {
		return orderedProduct;
	}

	public void setOrderedProduct(List<OrderedProduct> orderedProduct) {
		this.orderedProduct = orderedProduct;
	}

	public PaymentSummary getPaymentSummary() {
		return paymentSummary;
	}

	public void setPaymentSummary(PaymentSummary paymentSummary) {
		this.paymentSummary = paymentSummary;
	}

	public RefundAmountDetails getRefundAmountDetails() {
		return refundAmountDetails;
	}

	public void setRefundAmountDetails(RefundAmountDetails refundAmountDetails) {
		this.refundAmountDetails = refundAmountDetails;
	}

	public DeliveryAddress getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	
}

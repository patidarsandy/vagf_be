package com.vegf.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="payment_summary")
@JsonIgnoreProperties(ignoreUnknown = true,value = {"hibernateLazyInitializer", "handler" })
public class PaymentSummary {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id" )
	private Long id;

	@Column(name="payment_method", length=255)
	private String paymentMethod;

	@Column(name="transaction_id", length=255)
	private String transactionId;

	@Column(name="paid_amount", columnDefinition = "Decimal(18,4) default 0")
	private Double paidAmount;

	@Column(name="due_amount", columnDefinition = "Decimal(18,4) default 0")
	private Double dueAmount;

	@Column(name="payment_status", length=255)
	private String paymentStatus;

	@Column(name="total_amount", columnDefinition = "Decimal(18,4) default 0")
	private Double totalAmount ;

	@Column(name="product_amount", columnDefinition = "Decimal(18,4) default 0")
	private Double productAmount;
	
	@Column(name="product_discount_amount", columnDefinition = "Decimal(18,4) default 0")
	private Double productDiscountAmount;
	
	@Column(name="used_wallet_amount", columnDefinition = "Decimal(18,4) default 0")
	private Double usedWalletAmount ;

	@Column(name="delivery_charge", columnDefinition = "Decimal(18,4) default 0")
	private Double deliveryCharge ;
	
	@Column(name = "transaction_date", nullable = false, columnDefinition = "DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getProductAmount() {
		return productAmount;
	}

	public void setProductAmount(Double productAmount) {
		this.productAmount = productAmount;
	}

	public Double getProductDiscountAmount() {
		return productDiscountAmount;
	}

	public void setProductDiscountAmount(Double productDiscountAmount) {
		this.productDiscountAmount = productDiscountAmount;
	}

	public Double getUsedWalletAmount() {
		return usedWalletAmount;
	}

	public void setUsedWalletAmount(Double usedWalletAmount) {
		this.usedWalletAmount = usedWalletAmount;
	}

	public Double getDeliveryCharge() {
		return deliveryCharge;
	}

	public void setDeliveryCharge(Double deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	
}

package com.vegf.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="refund_amount_details" )
@JsonIgnoreProperties(ignoreUnknown = true,value = {"hibernateLazyInitializer", "handler" })
public class RefundAmountDetails {

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "id" )
		private Long id;

		@Column(name="payment_method", length=255)
		private String paymentMethod;

		@Column(name="transaction_id")
		private String transactionId;

		@Column(name="total_amount", columnDefinition = "Decimal(18,4) default 0")
		private Double totalAmount;

		@Column(name="cashback_amount", columnDefinition = "Decimal(18,4) default 0")
		private Double cashbackAmount;
		
		@Column(name = "initated_date", nullable = false, columnDefinition = "DATETIME")
		@Temporal(TemporalType.TIMESTAMP)
		private Date initatedDate;

		@Column(name="cashback_source")
		private String cashbackSource;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getPaymentMethod() {
			return paymentMethod;
		}

		public void setPaymentMethod(String paymentMethod) {
			this.paymentMethod = paymentMethod;
		}

		public String getTransactionId() {
			return transactionId;
		}

		public void setTransactionId(String transactionId) {
			this.transactionId = transactionId;
		}

		public Double getTotalAmount() {
			return totalAmount;
		}

		public void setTotalAmount(Double totalAmount) {
			this.totalAmount = totalAmount;
		}

		public Double getCashbackAmount() {
			return cashbackAmount;
		}

		public void setCashbackAmount(Double cashbackAmount) {
			this.cashbackAmount = cashbackAmount;
		}

		public Date getInitatedDate() {
			return initatedDate;
		}

		public void setInitatedDate(Date initatedDate) {
			this.initatedDate = initatedDate;
		}

		public String getCashbackSource() {
			return cashbackSource;
		}

		public void setCashbackSource(String cashbackSource) {
			this.cashbackSource = cashbackSource;
		}
		
		

		
}

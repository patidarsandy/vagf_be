package com.vegf.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="sub_category" )
@JsonIgnoreProperties(ignoreUnknown = true,value = {"hibernateLazyInitializer", "handler" })
public class SubCategory {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id" )
	private Long id;
	
	@Column(name="sub_category_name", length=255)
	private String subCategoryName;
	
	@OneToOne(fetch=FetchType.LAZY,orphanRemoval=false)
	@JoinColumn(name="fk_category_id")
	private Category category;
	
	@Column(name="sub_category_description", length=255)
	private String subCategoryDescription;
	
	@Column(name="icon_path", length=255)
	private String iconPath;
	
	@Column(name="active", columnDefinition = "tinyint(1) default 0")
	private Boolean active = false;

	@Column(name="discount", columnDefinition = "Decimal(18,4) default 0")
	private Double discount;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "fk_sub_category_id", referencedColumnName = "id")
    private List<SubCategoryImages> categoryImages = new ArrayList<>();

    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getSubCategoryDescription() {
		return subCategoryDescription;
	}

	public void setSubCategoryDescription(String subCategoryDescription) {
		this.subCategoryDescription = subCategoryDescription;
	}


	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public List<SubCategoryImages> getCategoryImages() {
		return categoryImages;
	}

	public void setCategoryImages(List<SubCategoryImages> categoryImages) {
		this.categoryImages = categoryImages;
	}
	
}

package com.vegf.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vegf.entities.auth.Role;

@Entity
@Table(name="user")
@JsonIgnoreProperties(ignoreUnknown = true,value = {"hibernateLazyInitializer", "handler" })
public class User  {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id" )
	private Long id;

	@Column(length=20,unique = true)
	private String username;
	
	@Column(length=60)
	private String password;
	
	@Column(name="default_account_id")
	private Long defaultAccountId;
	
	@Column(name="first_name",length=20)
	private String firstName;
	
	@Column(name="middle_name",length=20)
	private String middleName;
	
	@Column(name="last_name",length=20)
	private String lastName;

	@Column(name="mobile_no",length=20)
	private Long mobileNo;

	@Column(name="email",length=200)
	private String email;

	@Column(name = "last_login", nullable = false, columnDefinition = "DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;

	@OneToOne(fetch=FetchType.LAZY,orphanRemoval=false)
	@JoinColumn(name="fk_role_id")
	private Role role;

	@Column(name="otp")
	private Long otp;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "fk_user_id", referencedColumnName = "id")
    private List<DeliveryAddress> deliveryAddress = new ArrayList<>();

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getDefaultAccountId() {
		return defaultAccountId;
	}

	public void setDefaultAccountId(Long defaultAccountId) {
		this.defaultAccountId = defaultAccountId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Long getOtp() {
		return otp;
	}

	public void setOtp(Long otp) {
		this.otp = otp;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<DeliveryAddress> getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(List<DeliveryAddress> deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	
}

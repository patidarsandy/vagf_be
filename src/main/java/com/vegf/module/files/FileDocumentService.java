package com.vegf.module.files;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;



@Service
public class FileDocumentService {

	@Autowired
	private FileStorageProperties fileStorageProperties;

	private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd/");
	private DateFormat timeFormat = new SimpleDateFormat("hhmmss");

	public String saveToFS(MultipartFile  files, Long parentId, String classType) throws Exception {
		return uploadFile(files, parentId, classType);
	}
	
	public String uploadFile(MultipartFile file, Long parentId, String classType) throws Exception {
		// Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String validFileName = setValidFileName(fileName);
        System.out.println(validFileName);
        if(validFileName.contains("..")) {
            throw new Exception("Sorry! Filename contains invalid path sequence " + validFileName);
        }
        Date dt = new Date();
        String fileRelativePath = classType;  
//        fileRelativePath =  fileRelativePath + dateFormat.format(dt);
        
        String filePath = fileStorageProperties.getUploadDir() + fileRelativePath;
        Path fileStorageLocation = Paths.get(filePath)
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(fileStorageLocation);
        } catch (Exception ex) {
            throw new Exception("Could not create the directory where the uploaded files will be stored.", ex);
        }
        String strTime = timeFormat.format(dt);
        Path targetLocation = fileStorageLocation.resolve(strTime + "_" + validFileName);
        
        Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        return  fileRelativePath + strTime + "_" + validFileName;
        
    }
	
	public String setValidFileName(String filename) throws Exception {
		String file = filename.replace('+', '-');
		String afterRemoveDots = "";
		int fileNameLength = file.lastIndexOf(".");
		for (int i=0; i< file.length(); i++) {
			if (i < fileNameLength) {
				if( file.charAt(i) == '.') {
					afterRemoveDots = afterRemoveDots + "-";							
				}else {
					afterRemoveDots = afterRemoveDots + file.charAt(i);
				}
			}else {
				afterRemoveDots = afterRemoveDots + file.charAt(i);
			}
		}
		return afterRemoveDots;
	}
}

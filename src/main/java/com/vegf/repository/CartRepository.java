package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.Cart;

@Repository("CartRepository")
public interface CartRepository  extends CrudRepository<Cart, Long>{

	
}
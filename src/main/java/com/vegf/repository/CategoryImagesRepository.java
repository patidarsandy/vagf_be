package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.CategoryImages;


@Repository("CategoryImagesRepository")
public interface CategoryImagesRepository  extends CrudRepository<CategoryImages, Long>{

}
package com.vegf.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.Category;

@Repository("CategoryRepository")
public interface CategoryRepository  extends CrudRepository<Category, Long>{

	Category findByCategoryName(String categoryName);

	List<Category> findAll();

}

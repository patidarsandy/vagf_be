
package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.City;

@Repository("CityRepository")
public interface CityRepository  extends CrudRepository<City, Long>{


}


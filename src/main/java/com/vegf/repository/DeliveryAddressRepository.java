package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.DeliveryAddress;

@Repository("DeliveryAddressRepository")
public interface DeliveryAddressRepository  extends CrudRepository<DeliveryAddress, Long>{

}

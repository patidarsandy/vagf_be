package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.OrderedProduct;

@Repository("OrderedProductRepository")
public interface OrderedProductRepository  extends CrudRepository<OrderedProduct, Long>{

}

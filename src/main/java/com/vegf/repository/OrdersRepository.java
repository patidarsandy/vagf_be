package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.Orders;

@Repository("OrdersRepository")
public interface OrdersRepository  extends CrudRepository<Orders, Long>{

}

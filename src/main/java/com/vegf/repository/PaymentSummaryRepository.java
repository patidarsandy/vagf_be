package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.PaymentSummary;

@Repository("PaymentSummaryRepository")
public interface PaymentSummaryRepository  extends CrudRepository<PaymentSummary, Long>{

}

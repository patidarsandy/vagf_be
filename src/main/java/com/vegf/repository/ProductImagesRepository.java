package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.ProductImages;

@Repository("ProductImagesRepository")
public interface ProductImagesRepository  extends CrudRepository<ProductImages, Long>{

}

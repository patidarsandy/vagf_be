package com.vegf.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.Category;
import com.vegf.entities.Product;

@Repository("ProductRepository")
public interface ProductRepository  extends CrudRepository<Product, Long>{

	Product findByName(String name);

	List<Product> findAll();

}


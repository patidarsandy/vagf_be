package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.RefundAmountDetails;

@Repository("RefundAmountDetailsRepository")
public interface RefundAmountDetailsRepository  extends CrudRepository<RefundAmountDetails, Long>{

}

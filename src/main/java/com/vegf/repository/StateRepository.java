package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.State;

@Repository("StateRepository")
public interface StateRepository  extends CrudRepository<State, Long>{

}

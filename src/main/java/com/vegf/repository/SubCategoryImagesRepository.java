package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.SubCategoryImages;


@Repository("SubCategoryImagesRepository")
public interface SubCategoryImagesRepository  extends CrudRepository<SubCategoryImages, Long>{

}
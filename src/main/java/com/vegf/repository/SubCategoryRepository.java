package com.vegf.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.SubCategory;

@Repository("SubCategoryRepository")
public interface SubCategoryRepository  extends CrudRepository<SubCategory, Long>{

	SubCategory findBySubCategoryName(String subCategoryName);

	List<SubCategory> findAll();
}

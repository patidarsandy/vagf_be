package com.vegf.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.Unit;

@Repository("UnitRepository")
public interface UnitRepository  extends CrudRepository<Unit, Long>{

	List<Unit> findAll();
	
}

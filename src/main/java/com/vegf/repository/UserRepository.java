package com.vegf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vegf.entities.User;

@Repository("UserRepository")
public interface UserRepository  extends CrudRepository<User, Long>{

}

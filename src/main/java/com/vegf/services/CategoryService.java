package com.vegf.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vegf.entities.Category;
import com.vegf.entities.CategoryImages;
import com.vegf.module.files.FileDocumentService;
import com.vegf.repository.CategoryImagesRepository;
import com.vegf.repository.CategoryRepository;
import com.vegf.vm.MessageVM;

@Service("CategoryService")
public class CategoryService {
	
	
	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	FileDocumentService fileDocumentService;
	
	@Autowired
	CategoryImagesRepository categoryImagesRepository;
	
	
//	public MessageVM createCategory(Category category) throws Exception {
//		MessageVM  messageVM = new MessageVM(); 
//		if ( category.getCategoryName() != null ) {
//			Category oldCat = categoryRepository.findByCategoryName(category.getCategoryName());
//			if ( oldCat != null ) {
//				throw new Exception("Category with this name already present.");
//			} else {
//				categoryRepository.save(category);
//				messageVM.setCode("200");
//				messageVM.setData(category);
//				messageVM.setMessage("Successfully category created.");
//			}
//		}
//		return messageVM;
//	}
	
	public MessageVM getAllCategory() throws Exception {
		MessageVM  messageVM = new MessageVM(); 		
		try {
			List<Category> category = categoryRepository.findAll();
			messageVM.setCode("200");
			messageVM.setData(category);
			messageVM.setMessage("Successfully category created.");
		} catch (Exception e) {
			messageVM.setCode("500");
			messageVM.setData(null);
			messageVM.setMessage(e.getMessage());
		}
		return messageVM;
	}

	public MessageVM createCategory(String model, MultipartFile file, List<MultipartFile> files) throws Exception {
		MessageVM  messageVM = new MessageVM(); 
		if ( Objects.nonNull(model)) {
			ObjectMapper mapper = new ObjectMapper();
			Category category = mapper.readValue(model, Category.class);
			Category oldCat = categoryRepository.findByCategoryName(category.getCategoryName());
			if ( oldCat != null ) {
				throw new Exception("Category with this name already present.");
			} else {
				categoryRepository.save(category);
				List<CategoryImages> images = new ArrayList<>();
				try {
					if(files.size() > 0) {
						for(MultipartFile catFile : files) {
							String filename = catFile.getOriginalFilename();
							String validFileName = fileDocumentService.setValidFileName(filename);
							String locationPath = fileDocumentService.saveToFS(catFile, category.getId() , "CategoryImages");
							CategoryImages document = new CategoryImages();
							document.setDocumentId(UUID.randomUUID().toString());
							document.setPath(locationPath);
							document.setFileType(catFile.getContentType());
							document.setName(validFileName);
							document.setType("Category");
							images.add(document);
						}
					}
					if(Objects.nonNull(file)) {
						String locationPath = fileDocumentService.saveToFS(file, category.getId() , "CategoryIcon");
						category.setIconPath(locationPath);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				if(images.size() > 0) {
					category.setCategoryImages(images);
				}
				categoryRepository.save(category);
				messageVM.setCode("200");
				messageVM.setData(category);
				messageVM.setMessage("Successfully category created.");
			}
		}
		return messageVM;
	}

	public MessageVM saveCategory(Category category) throws Exception {
		MessageVM  messageVM = new MessageVM(); 
		if ( category.getCategoryName() != null ) {
			Optional<Category> oldCat = categoryRepository.findById(category.getId());
			if ( !oldCat.isPresent()  ) {
				throw new Exception("Category not found.");
			} else {
				Category sameNameCat = categoryRepository.findByCategoryName(category.getCategoryName());
				if (sameNameCat != null && !sameNameCat.getId().equals(oldCat.get().getId())) {
					throw new Exception("You can't update with this name.");
				}
				categoryRepository.save(category);
				messageVM.setCode("200");
				messageVM.setData(category);
				messageVM.setMessage("Successfully category Updated.");
			}
		}
		return messageVM;
	}

	public MessageVM updateCategoryStatus(Long catId, Boolean active) throws Exception {
		MessageVM  messageVM = new MessageVM(); 
		if ( catId != null ) {
			Optional<Category> oldCat = categoryRepository.findById(catId);
			if ( oldCat.isPresent()  ) {
				Category category = oldCat.get();
				category.setActive(active);
				categoryRepository.save(category);
				messageVM.setCode("200");
				messageVM.setData(category);
				messageVM.setMessage("Successfully category Updated.");
			} else {
				throw new Exception("Category not found.");
			}
		} else {
			throw new Exception("Category id required .");
		}
		return messageVM;
	}

}

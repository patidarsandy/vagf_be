package com.vegf.services;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vegf.dao.LoginDao;
import com.vegf.entities.auth.AuthUser;
import com.vegf.vm.MessageVM;
import com.vegf.vm.UserProfileVM;


@Service("LoginService")
@Transactional
public class LoginService {
	@Autowired
	LoginDao loginDao;

	public MessageVM createUser(UserProfileVM profileVM){
		MessageVM messageVM = new MessageVM();
		AuthUser authUser = new AuthUser();
		authUser =	loginDao.getAuthUserByEmailId(profileVM.getEmail());
		if(authUser == null)
		{
			authUser =new AuthUser();
			authUser.setEmail(profileVM.getEmail());
			authUser.setUsername(profileVM.getEmail());
			authUser.setType(profileVM.getType());
			authUser.setPassword(profileVM.getPassword());
			authUser.setLastName(profileVM.getLastName());
			authUser.setFirstName(profileVM.getFirstName());
			authUser.setCreatedDate(new Date());
			authUser.setEnabled(true);
			loginDao.createUser(authUser);
			messageVM.setMessage("User Successfully Created");
		}else{
			messageVM.setMessage("User already Present");
		}
		return messageVM;
	}
	
}

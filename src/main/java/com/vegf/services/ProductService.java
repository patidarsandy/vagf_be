package com.vegf.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vegf.entities.Product;
import com.vegf.entities.ProductImages;
import com.vegf.module.files.FileDocumentService;
import com.vegf.repository.ProductImagesRepository;
import com.vegf.repository.ProductRepository;
import com.vegf.vm.MessageVM;

@Service("ProductService")
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	FileDocumentService fileDocumentService;
	
	@Autowired
	ProductImagesRepository productImagesRepository;

	public MessageVM getAllProduct() throws Exception {
		MessageVM  messageVM = new MessageVM(); 		
		try {
			List<Product> product = productRepository.findAll();
			messageVM.setCode("200");
			messageVM.setData(product);
			messageVM.setMessage("Successfully Product created.");
		} catch (Exception e) {
			messageVM.setCode("500");
			messageVM.setData(null);
			messageVM.setMessage(e.getMessage());
		}
		return messageVM;
	}

	public MessageVM addProduct(String model, MultipartFile file, List<MultipartFile> files) throws Exception {
		MessageVM  messageVM = new MessageVM(); 
		if ( Objects.nonNull(model)) {
			ObjectMapper mapper = new ObjectMapper();
			Product product = mapper.readValue(model, Product.class);
			Product oldProduct = productRepository.findByName(product.getName());
			if ( oldProduct != null ) {
				throw new Exception("Product with this name already present.");
			} else {
				productRepository.save(product);
				List<ProductImages> images = new ArrayList<>();
				try {
					if(files.size() > 0) {
						for(MultipartFile catFile : files) {
							String filename = catFile.getOriginalFilename();
							String validFileName = fileDocumentService.setValidFileName(filename);
							String locationPath = fileDocumentService.saveToFS(catFile, product.getId() , "ProductImages");
							ProductImages document = new ProductImages();
							document.setDocumentId(UUID.randomUUID().toString());
							document.setPath(locationPath);
							document.setFileType(catFile.getContentType());
							document.setName(validFileName);
							document.setType("Product");
							images.add(document);
						}
					}
					if(Objects.nonNull(file)) {
						String locationPath = fileDocumentService.saveToFS(file, product.getId() , "ProductIcon");
						product.setIconPath(locationPath);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				if(images.size() > 0) {
					product.setProductImages(images);
				}
				productRepository.save(product);
				messageVM.setCode("200");
				messageVM.setData(product);
				messageVM.setMessage("Successfully category created.");
			}
		}
		return messageVM;
	}

}

package com.vegf.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vegf.entities.Category;
import com.vegf.entities.CategoryImages;
import com.vegf.entities.SubCategory;
import com.vegf.entities.SubCategoryImages;
import com.vegf.module.files.FileDocumentService;
import com.vegf.repository.SubCategoryImagesRepository;
import com.vegf.repository.SubCategoryRepository;
import com.vegf.vm.MessageVM;

@Service("SubCategoryService")
public class SubCategoryService {
	
	
	@Autowired
	SubCategoryRepository subCategoryRepository;


	@Autowired
	FileDocumentService fileDocumentService;
	
	@Autowired
	SubCategoryImagesRepository subCategoryImagesRepository;

	
	
	public MessageVM getAllSubCategory() throws Exception {
		MessageVM  messageVM = new MessageVM(); 		
		try {
			List<SubCategory> category = subCategoryRepository.findAll();
			messageVM.setCode("200");
			messageVM.setData(category);
			messageVM.setMessage("Successfully category created.");
		} catch (Exception e) {
			messageVM.setCode("500");
			messageVM.setData(null);
			messageVM.setMessage(e.getMessage());
		}
		return messageVM;
	}
	
	public MessageVM createSubCategory(String model, MultipartFile file, List<MultipartFile> files) throws Exception {
		MessageVM  messageVM = new MessageVM(); 
		if ( Objects.nonNull(model)) {
			ObjectMapper mapper = new ObjectMapper();
			SubCategory subCategory = mapper.readValue(model, SubCategory.class);
			SubCategory oldCat = subCategoryRepository.findBySubCategoryName(subCategory.getSubCategoryName());
			if ( oldCat != null ) {
				throw new Exception("Sub Category with this name already present.");
			} else {
				subCategoryRepository.save(subCategory);
				List<SubCategoryImages> images = new ArrayList<>();
				try {
					if(files.size() > 0) {
						for(MultipartFile catFile : files) {
							String filename = catFile.getOriginalFilename();
							String validFileName = fileDocumentService.setValidFileName(filename);
							String locationPath = fileDocumentService.saveToFS(catFile, subCategory.getId() , "CategoryImages");
							SubCategoryImages document = new SubCategoryImages();
							document.setDocumentId(UUID.randomUUID().toString());
							document.setPath(locationPath);
							document.setFileType(catFile.getContentType());
							document.setName(validFileName);
							document.setType("SubCategory");
							images.add(document);
						}
					}
					if(Objects.nonNull(file)) {
						String locationPath = fileDocumentService.saveToFS(file, subCategory.getId() , "SubCategoryIcon");
						subCategory.setIconPath(locationPath);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				if(images.size() > 0) {
					subCategory.setCategoryImages(images);
				}
				subCategoryRepository.save(subCategory);
				messageVM.setCode("200");
				messageVM.setData(subCategory);
				messageVM.setMessage("Successfully category created.");
			}
		}
		return messageVM;
	}

	
	public MessageVM createSubCategory(SubCategory subCategory) throws Exception {
		MessageVM  messageVM = new MessageVM(); 
		if ( subCategory.getSubCategoryName() != null ) {
			SubCategory oldSubCat = subCategoryRepository.findBySubCategoryName(subCategory.getSubCategoryName());
			if ( oldSubCat != null ) {
				throw new Exception("Sub Category with this name already present.");
			} else {
				subCategoryRepository.save(subCategory);
				messageVM.setCode("200");
				messageVM.setData(subCategory);
				messageVM.setMessage("Successfully category created.");
			}
		}
		return messageVM;
	}

	public MessageVM saveSubCategory(SubCategory subCategory) throws Exception {
		MessageVM  messageVM = new MessageVM(); 
		if ( subCategory.getSubCategoryName() != null ) {
			Optional<SubCategory> oldCat = subCategoryRepository.findById(subCategory.getId());
			if ( !oldCat.isPresent()  ) {
				throw new Exception("Sub Category not found.");
			} else {
				SubCategory sameNameCat = subCategoryRepository.findBySubCategoryName(subCategory.getSubCategoryName());
				if (sameNameCat != null && !sameNameCat.getId().equals(oldCat.get().getId())) {
					throw new Exception("You can't update with this name.");
				}
				subCategoryRepository.save(subCategory);
				messageVM.setCode("200");
				messageVM.setData(subCategory);
				messageVM.setMessage("Successfully sub category Updated.");
			}
		}
		return messageVM;
	}

	public MessageVM updateSubCategoryStatus(Long subCatId, Boolean active) throws Exception {
		MessageVM  messageVM = new MessageVM(); 
		if ( subCatId != null ) {
			Optional<SubCategory> oldSubCat = subCategoryRepository.findById(subCatId);
			if ( oldSubCat.isPresent()  ) {
				SubCategory category = oldSubCat.get();
				category.setActive(active);
				subCategoryRepository.save(category);
				messageVM.setCode("200");
				messageVM.setData(category);
				messageVM.setMessage("Successfully sub category status Updated.");
			} else {
				throw new Exception("Sub Category not found.");
			}
		} else {
			throw new Exception("Sub Category id required .");
		}
		return messageVM;
	}

}

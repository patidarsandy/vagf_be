package com.vegf.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vegf.entities.Unit;
import com.vegf.repository.UnitRepository;
import com.vegf.vm.MessageVM;

@Service("UnitService")
public class UnitService {

	@Autowired
	UnitRepository unitRepository;
	
	public MessageVM getAllUnit() throws Exception {
		MessageVM  messageVM = new MessageVM(); 		
		try {
			List<Unit> unit = unitRepository.findAll();
			messageVM.setCode("200");
			messageVM.setData(unit);
			messageVM.setMessage("Successfully category created.");
		} catch (Exception e) {
			messageVM.setCode("500");
			messageVM.setData(null);
			messageVM.setMessage(e.getMessage());
		}
		return messageVM;
	}
}
